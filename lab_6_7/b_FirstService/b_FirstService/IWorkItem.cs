﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace b_FirstService
{
    [ServiceContract]
    public interface IWorkItem
    {
        [OperationContract]
        void AddItems(Item good);
        [OperationContract]
        void DelItem(int id);
        [OperationContract]
        List<Item> GetItems();
    }
}
