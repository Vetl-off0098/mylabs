﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace b_FirstService
{
    [DataContract]
    public class Item
    {
        [DataMember]
        public int id;
        [DataMember]
        public string name;
        [DataMember]
        public int price;
        [DataMember]
        public int quantity;
        [DataMember]
        public DateTime create;
    }
}
