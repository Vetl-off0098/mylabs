﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace b_FirstService
{
[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class WorkItem : IWorkItem
    {
        public int global_id = 0;
        public List<Item> items = new List<Item>();
        public List<Item> GetItems()
        {
            return items;
        }
        public void AddItems(Item good)
        {
            global_id++;
            good.id = global_id;
            items.Add(good);
        }
        public void DelItem(int id)
        {
            items.Remove(items.Find(e => e.id.Equals(id)));
        }
    }
}
