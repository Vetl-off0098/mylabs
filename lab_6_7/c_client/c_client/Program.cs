﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using c_client.WorkItemRef;

namespace c_client
{
    class Program
    {
        static void Main(string[] args)
        {
            WorkItemClient workitem = new WorkItemClient("BasicHttpBinding_IWorkItem");
            int global_id = 0;
            string action = "0";
            Console.WriteLine("Дообрый день!");
            while (action != "4")
            {
                Console.WriteLine(
                    "Что вы хотите сделать:\n" +
                    "1 - Добавить товар\n" +
                    "2 - Посмотреть все товары\n" +
                    "3 - Удалить товар\n" +
                    "4 - Выход"
                    );
                action = Console.ReadLine();
                if (action == "1")
                {
                    Item good = new Item();
                    Console.WriteLine("Введите название товара");
                    good.name = Console.ReadLine();
                    Console.WriteLine("Введите цену товара");
                    good.price = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Введите количество товара");
                    good.quantity = Convert.ToInt32(Console.ReadLine());
                    good.create = DateTime.Now;
                    workitem.AddItems(good);
                    Console.WriteLine("Товар успешно добавлен");
                }
                else if (action == "2")
                {
                    Console.Clear();
                    Item[] spisok = workitem.GetItems();
                    if (spisok.Length > 0)
                    {
                        foreach (Item item in spisok)
                        {
                            Console.WriteLine("№" + item.id + ":" + item.name + " \n");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Товаров в базе данных нет\n");
                    }
                }
                else if (action == "3")
                {
                    Console.WriteLine("Введите ID товара");
                    string id = Console.ReadLine();
                    workitem.DelItem(Convert.ToInt32(id));
                    Console.WriteLine("Товар успешно удален");
                }
            }
        }
    }
}
