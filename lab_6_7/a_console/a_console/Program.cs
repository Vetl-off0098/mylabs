﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace a_console
{
    public class Item
    {
        public int id;
        public string name;
        public int price;
        public int quantity;
        public DateTime create;
    }

    public class WorkItem
    {
        public int global_id = 0;
        public List<Item> items = new List<Item>();
        public List<Item> GetItems()
        {
            return items;
        }
        public void AddItems(Item good)
        {
            global_id++;
            good.id = global_id;
            items.Add(good);
        }
        public void DelItem(int id)
        {
            items.Remove(items.Find(e => e.id.Equals(id)));
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            WorkItem workitem = new WorkItem();
            int global_id = 0;
            string action = "0";
            Console.WriteLine("Дообрый день!");
            while (action != "4")
            {
                Console.WriteLine(
                    "Что вы хотите сделать:\n" +
                    "1 - Добавить товар\n" + 
                    "2 - Посмотреть все товары\n" +
                    "3 - Удалить товар\n" +
                    "4 - Выход"
                    );
                action = Console.ReadLine();
                if (action == "1")
                {
                    Item good = new Item();
                    Console.WriteLine("Введите название товара");
                    good.name = Console.ReadLine();
                    Console.WriteLine("Введите цену товара");
                    good.price = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Введите количество товара");
                    good.quantity = Convert.ToInt32(Console.ReadLine());
                    good.create = DateTime.Now;
                    workitem.AddItems(good);
                    Console.WriteLine("Товар успешно добавлен");
                }
                else if (action == "2")
                {
                    Console.Clear();
                    List<Item> spisok = workitem.GetItems();
                    if (spisok.Count > 0)
                    {
                        foreach(Item item in spisok)
                        {
                            Console.WriteLine("№" + item.id + ":" + item.name + " \n");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Товаров в базе данных нет\n");
                    }
                }
                else if (action == "3")
                {
                    Console.WriteLine("Введите ID товара");
                    string id = Console.ReadLine();
                    workitem.DelItem(Convert.ToInt32(id));
                    Console.WriteLine("Товар успешно удален");
                }
            }
        }
    }
}
