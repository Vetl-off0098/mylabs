﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;

namespace socketServer
{
    class Program
    {
        static void Main(string[] args)
        {
            IPHostEntry ipHost = Dns.GetHostEntry("localhost");
            IPAddress ipAddr = ipHost.AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 11000);
            Socket socketListener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                socketListener.Bind(ipEndPoint);
                socketListener.Listen(10);
                while (true)
                {
                    Console.WriteLine("Ожидание соединения через {0}", ipEndPoint);
                    Socket handler = socketListener.Accept();
                    Thread thr = new Thread(new ParameterizedThreadStart(Clients));
                    thr.Start(handler);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Console.ReadLine();
            }
        }
        public static class Items
        {
            public static string path = "./Vetlov.txt";



            public static List<string> items = new List<string>();
            static Mutex onceMute = new Mutex();
            public static bool Add(string text)
            {
                try
                {
                    onceMute.WaitOne();
                    using (StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.Default))
                    {
                        sw.WriteLine(text);
                    }
                    onceMute.ReleaseMutex();
                    return true;
                }
                catch
                {
                    onceMute = new Mutex();
                    return false;
                }
            }
            public static bool Delete(int numb)
            {
                try
                {
                    onceMute.WaitOne();
                    List<string> lst = new List<string>();
                    using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
                    {
                        int index = 0;
                        string line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            if (index != numb)
                            {
                                lst.Add(line);
                            }
                            index++;
                        }
                    }
                    using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                    {
                        foreach (string elem in lst)
                        {
                            sw.WriteLine(elem);
                        }
                    }
                    onceMute.ReleaseMutex();
                    return true;
                }
                catch
                {
                    onceMute = new Mutex();
                    return false;
                }
            }
            public static string listProg()
            {
                try
                {
                    onceMute.WaitOne();
                    string lst = "";
                    using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
                    {
                        int index = 0;
                        string line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            lst = lst + Convert.ToString(index) + "-" + line + "\n";
                            index++;
                        }
                    }
                    onceMute.ReleaseMutex();
                    if (lst == "")
                        throw new Exception();
                    return lst;
                }
                catch
                {
                    onceMute = new Mutex();
                    return "Список товаров пуст";
                }
            }
        }
        public static void Comands(string command, Socket sckt)
        {
            if (command == "Add")
            {
                string answer = "Введите название товара";
                byte[] msg = Encoding.UTF8.GetBytes(answer);
                sckt.Send(msg);
                string data = null;
                byte[] bytes = new byte[1024];
                int bytesRec = sckt.Receive(bytes);
                data += Encoding.UTF8.GetString(bytes, 0, bytesRec);
                bool Total = Items.Add(data);
                answer = "Товар " + data + " успешно добавлен";
                msg = Encoding.UTF8.GetBytes(answer);
                sckt.Send(msg);
            }
            if (command == "List")
            {
                string answer = "";
                byte[] msg = Encoding.UTF8.GetBytes(answer);
                byte[] bytes = new byte[1024];
                string text = Items.listProg();
                answer = text;
                msg = Encoding.UTF8.GetBytes(answer);
                sckt.Send(msg);
            }
            if (command == "Delete")
            {
                string answer = "Введите id товара для удаления";
                byte[] msg = Encoding.UTF8.GetBytes(answer);
                sckt.Send(msg);
                string data = null;
                byte[] bytes = new byte[1024];
                int bytesRec = sckt.Receive(bytes);
                data += Encoding.UTF8.GetString(bytes, 0, bytesRec);
                string str = data;
                int num;
                bool isNum = int.TryParse(str, out num);
                if (isNum)
                {
                    bool itog = Items.Delete(num);
                    answer = "Товар " + data + " успешно удален";
                    msg = Encoding.UTF8.GetBytes(answer);
                    sckt.Send(msg);
                }
                else
                {
                    Console.Write("Неудача: " + "\n\n");
                    answer = "Ошибка";
                    msg = Encoding.UTF8.GetBytes(answer);
                    sckt.Send(msg);
                }
            }
        }
        public static void Clients(Object drain)
        {
            Socket handler = (Socket)drain;
            try
            {
                while (true)
                {
                    string data = null;
                    byte[] bytes = new byte[1024];
                    int bytesRec = handler.Receive(bytes);
                    data += Encoding.UTF8.GetString(bytes, 0, bytesRec);
                    Comands(data, handler);
                    Console.Write("Результат: " + data + "\n");
                    if (data.IndexOf("<TheEnd>") > -1)
                    {
                        Console.WriteLine("Сервер завершил соединение с клиентом.");
                        break;
                    }
                    Thread.Sleep(1000);
                }
                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
            }
            finally
            {
                Console.ReadLine();
                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
            }
        }
    }
}
