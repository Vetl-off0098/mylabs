﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace socketClient
{
    class Program
    {
        static void StartServer()
        {
            try
            {
                IPHostEntry ipHost = Dns.GetHostEntry("localhost");
                IPAddress ipAddr = ipHost.AddressList[0];

                IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 11000);
                Socket sender = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                sender.Connect(ipEndPoint);
                SendMessageFromSocket(sender);
            }
            catch
            {
                Error();
            }
        }
        static void SendMessageFromSocket(Socket sender)
        {
            try
            {
                byte[] bytes = new byte[1024];
                Console.Write(
                    "Введите:\n" +
                    "1 - Для добавления товара \n" +
                    "2 - Для удаления товара \n" +
                    "3 - Для вывода списка товаров \n"
                    );
                string message = Console.ReadLine();
                switch (message)
                {
                    case "1":
                        Add(sender);
                        break;
                    case "2":
                        Delete(sender);
                        break;
                    case "3":
                        List(sender);
                        break;
                }
                if (message.IndexOf("<TheEnd>") == -1)
                {
                    SendMessageFromSocket(sender);
                }
                sender.Shutdown(SocketShutdown.Both);
                sender.Close();
            }
            catch
            {
                Error();
            }
        }
        static void Add(Socket sender)
        {
            try
            {
                byte[] bytes = new byte[1024];
                byte[] msg = Encoding.UTF8.GetBytes("Add");
                int bytesSent = sender.Send(msg);
                int bytesRec = sender.Receive(bytes);
                Console.WriteLine("{0}\n", Encoding.UTF8.GetString(bytes, 0, bytesRec));
                string message = Console.ReadLine();
                if (message == "")
                {
                    message = "?";
                }
                msg = Encoding.UTF8.GetBytes(message);
                bytesSent = sender.Send(msg);
                bytesRec = sender.Receive(bytes);
                Console.WriteLine("{0}\n", Encoding.UTF8.GetString(bytes, 0, bytesRec));
            }
            catch
            {
                Error();
            }
        }
        static void Delete(Socket sender)
        {
            try
            {
                byte[] bytes = new byte[1024];
                byte[] msg = Encoding.UTF8.GetBytes("Delete");
                int bytesSent = sender.Send(msg);
                int bytesRec = sender.Receive(bytes);
                Console.WriteLine("{0}\n", Encoding.UTF8.GetString(bytes, 0, bytesRec));
                string message = Console.ReadLine();
                if (message == "")
                {
                    message = "?";
                }
                msg = Encoding.UTF8.GetBytes(message);
                bytesSent = sender.Send(msg);
                bytesRec = sender.Receive(bytes);
                Console.WriteLine("{0}\n", Encoding.UTF8.GetString(bytes, 0, bytesRec));
            }
            catch
            {
                Error();
            }
        }
        static void List(Socket sender)
        {
            try
            {
                byte[] msg = Encoding.UTF8.GetBytes("List");
                int bytesSent = sender.Send(msg);
                byte[] bytes = new byte[1024];
                int bytesRec = sender.Receive(bytes);
                Console.WriteLine("Список товаров: {0}\n", Encoding.UTF8.GetString(bytes, 0, bytesRec));
            }
            catch
            {
                Error();
            }
        }
        static void Main(string[] args)
        {
            try
            {
                StartServer();
            }
            catch
            {
                Error();
            }
        }
        static void Error()
        {
            Console.WriteLine("Сервер больше не работает");
        }
    }
}
